package com.niit.myrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import com.niit.myrest.jwtfilter.MyJwt;

@SpringBootApplication
public class MongExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(MongExampleApplication.class, args);
	}

	@Bean
	
	public CorsFilter cors()
	{
		UrlBasedCorsConfigurationSource source=new UrlBasedCorsConfigurationSource();
		CorsConfiguration config=new CorsConfiguration();
		config.setAllowCredentials(true);
		config.addAllowedOrigin("*");
		config.addAllowedHeader("*");
		config.addAllowedMethod("*");
		source.registerCorsConfiguration("/**", config);
		return new CorsFilter(source);
	}
	
	
	@Bean
	
	public FilterRegistrationBean mybean()
	{
		UrlBasedCorsConfigurationSource source=new UrlBasedCorsConfigurationSource();
		CorsConfiguration config=new CorsConfiguration();
		config.setAllowCredentials(true);
		config.addAllowedOrigin("*");
		config.addAllowedHeader("*");
		config.addAllowedMethod("*");
		source.registerCorsConfiguration("/**", config);
		
		final FilterRegistrationBean filterbean=new FilterRegistrationBean(new CorsFilter(source));
		
		filterbean.setFilter(new MyJwt());
		filterbean.addUrlPatterns("/api/mongo/showall");
		
		return filterbean;
	}
	
	
}

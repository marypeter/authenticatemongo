FROM java:8-jre
WORKDIR usr/src
ENV MONGO_URL=mongodb://localhost:27017/ctsdemo
ADD ./target/MongExample-0.0.1-SNAPSHOT.jar /usr/src/MongExample-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","MongExample-0.0.1-SNAPSHOT.jar"]